import { EventEmitter } from "events"
EventEmitter.prototype.setMaxListeners(512);

import http from "http"
import path from "path"

import "colors" // enable text color in console (globaly)
import "./config/environment/globals.js" // Load globals

import config from "./config/environment"
import expressConfig from "./config/express"
import "./websocket" // Start the websocket server

process.on("warning", (err) => {
    console.warn(err.stack);
})

// Setup express
expressConfig(config)
.then((app) => {
    // Setup http server
    return {
        app,
        server: http.createServer(app),
    }
})
.then((data) => {
    // Start server
    const {
        server,
        app
    } = data;

    return new Promise((res, rej) => {
        app.server = server.listen(config.port, config.ip, function(err) {
            if(err){
                rej(err)
            } else {
                res(data);
            }
        })
    })
})
.then((data) => {
    console.log("Express server listening on ".cyan + (config.ip + ":" + config.port).green + ", in ".cyan + __ENV__.green + " mode".cyan)
})
.catch((err) => {
    console.log("There was an uncatch error : ".red);
    // console.error(err.message);
    console.error(err);
})
