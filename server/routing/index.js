"use strict"

import { Router } from "express"
import promiseSettle from "promise-settle"
import axios from "axios";
import pathLib from "path"

const routes = Router();

routes.get("/ping", (req, res) => {
    res.json({time: Date.now()})
})

export default routes;
