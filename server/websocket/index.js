
import ws from "ws"
import sio from "socket.io"
import { Script } from "vm";
import { setInterval } from "timers";


var currentAi;

const sioServer = sio()
sioServer.listen(3003);

const wsServerMbed = new ws.Server({
    port:3002,
})

const StateEnum = {
    RED: 0,
    GREEN: 1,
    YELLOW: 2,
}


// Light algo

class LightMbedAI {

    constructor(sioServer, wsServer) {

        this._sioServer = sioServer;
        this._wsServer = wsServer;

        this._sioWebAppClient;
        this._sioDetectionClient;
        this._wsClient;

        this._timestamps = [Date.now(), Date.now()];
        this._vehiclesCount = [0, 0];

        this.currentLightIndex = 0;
        this.currentLightState = StateEnum.RED;
        this._priorityIndex = -1;

        this._setupSockets();

        this._main();
    }

    _setupSockets() {

        wsServerMbed.on("connection", (wsClient) => {
            this._wsClient = wsClient;

            this._wsClient.on("message", (raw) => {
                const data = String(raw);

                if(data == "Xbee") {
                    // nothing
                } else {
                    this.givePriority(data);
                }
            })

        })

        this._sioServer.on("connection", (sioClient) => {

            sioClient.on("DETECTION_CLIENT", () => {
                console.log("DETECTION_CLIENT CONNECTED");
                this._sioDetectionClient = sioClient;
            })
            sioClient.on("DETECTION", this.setVehicleCount.bind(this))


            sioClient.on("GET_INITIAL_STATE", (intersectionId, callback) => {
                console.log("WEBAPP CONNECTED");
                callback(this._getCurrentState());
                this._sioWebAppClient = sioClient;
            })

            sioClient.on("MANUAL_UPDATE", (camId, callback) => {

                // console.log("PASS")

                // setTimeout(() => {
                //     callback(StateEnum.GREEN);
                // }, 1000);

                this.givePriority(camId);


            })

        })
    }

    async _main() {

        //Main loop

        if(this._priorityIndex >= 0) {
            await this._nextLight(this._priorityIndex);
            this._priorityIndex = -1;
        } else if(this._getTimeForCam(this.currentLightIndex) > 5000){

            var priority_1 = this._getTimeForCam(0) / 1000; // cam 1
            var priority_2 = this._getTimeForCam(1) / 1000; // cam 2

            priority_1 += this._vehiclesCount[0];
            priority_2 += this._vehiclesCount[1];

            // console.log("----")
            // console.log(priority_1)
            // console.log(priority_2)

            if(priority_1 > priority_2) {
                if(this.currentLightIndex != 0) {
                    await this._nextLight(0);
                }
            } else {
                if(this.currentLightIndex != 1) {
                    await this._nextLight(1);
                }
            }

        }


        setTimeout(this._main.bind(this), 100) // bad ?

    }

    async _nextLight(nextCamId) {
        return new Promise((res, rej) => {

            this.currentLightState = StateEnum.YELLOW;
            this._updateStateForCam(this.currentLightIndex, this.currentLightState)

            setTimeout(() => {

                this.currentLightState = StateEnum.RED;
                this._updateStateForCam(this.currentLightIndex, this.currentLightState)


                this.currentLightIndex = nextCamId;
                this.currentLightState = StateEnum.GREEN;
                this._updateStateForCam(this.currentLightIndex, this.currentLightState)

                this._refreshTimestampForCam(nextCamId)
                res();

            }, 1000);
        })
    }

    givePriority(camId) {
        // console.log(camId   )
        this._priorityIndex = camId;
    }

    setVehicleCount(camId, nb) {
        this._vehiclesCount[camId] = nb;
    }

    _getTimeForCam(camId) {
        return Date.now() - this._timestamps[camId];
    }

    _refreshTimestampForCam(camId) {
        this._timestamps[camId] = Date.now();
    }

    _updateStateForCam(camId, camState) {
        this._updateWebAppStateForCam(camId, camState)
        this._updateXbeeState(camId, camState)
    }

    _updateWebAppStateForCam(camId, camState) {
        if(this._sioWebAppClient) {
            this._sioWebAppClient.emit("UPDATE_CAM_STATE", camId, camState)
        } else {
            console.log("WebApp not connected")
        }
    }

    _updateXbeeState(camId, camState) {
        if(this._wsClient && this._wsClient.readyState == ws.OPEN) {
            this._wsClient.send(camId + ":" + camState)
        } else {
            // console.log("Xbee not connected")
        }
    }

    _getCurrentState() {
        return {
            0: StateEnum.RED,
            1: StateEnum.RED,
            2: StateEnum.RED,
            3: StateEnum.RED,
            ... {
                [this.currentLightIndex] : this.currentLightState
            }
        }
    }


}

currentAi = new LightMbedAI(sioServer, wsServerMbed);
