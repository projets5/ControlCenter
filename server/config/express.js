"use strict";

/*
    Express config
*/


import express from "express"
import morgan from "morgan"
import bodyParser from "body-parser"
import errorHandler from "errorhandler"
import pathLib from "path"
import fs from "fs"

import routes from "../routing"

export default function(config) {
    const app = express();
    const env = app.get("env");

    if(__DEV__) {
        // Logger
        app.use(morgan("dev"));
    }

    // BodyParser
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(bodyParser.json({limit: "50mb"}));

    // Routes
    app.use("/ctrlcenter", routes);

    // Error handler - must be last
    if(__DEV__) {
        app.use(errorHandler());
    }

    return Promise.resolve(app);
}
