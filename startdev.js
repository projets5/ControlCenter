// This file hook babel to require so we can use babel dynamicaly
// IT MOST NOT BE USE IN PROD
require("babel-register");

// Set the max number of listeners an EventEmitter can drive..
// Patch for formidable :
// https://github.com/felixge/node-formidable/issues/422
require('events').EventEmitter.prototype.setMaxListeners(512)

require("./server/server.js");
